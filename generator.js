/**
* Settings
*/
var path = window.location.href.split('?')[0] + "resources/" // path to resources (logos, images, ...). 
	// Relative to location of the generator but will be absolute in the generated signature.

/**
 * Once the page is loaded, set up the event listeners of the buttons create initial preview
 */
window.onload = function () {

    //store original url+query
    originalurl = window.location.href;

    //defining DOM elements (for global use)
    contactRowContainer = document.getElementById("contact-row-container");
    form = document.getElementById("form");
    original = document.getElementById("htmloriginal");
    result = document.getElementById("htmlresult");
    iframe = document.getElementById("iframeresult");

    // adding event listeners
    document.getElementById("updatebutton").addEventListener("click", update);
    document.getElementById("htmlbutton").addEventListener("click", copyhtml);
    document.getElementById("gmailbutton").addEventListener("click", copygmail);
    document.getElementById("addbutton").addEventListener("click", function () { addRow() });
    document.getElementById("generatorbutton").addEventListener("click", copyGeneratorLink);
    document.getElementById("resetbutton").addEventListener("click", resetForm);

    // button user feedback
    document.getElementById("updatebutton").addEventListener("click", greenbutton);
    document.getElementById("htmlbutton").addEventListener("click", greenbutton);
    document.getElementById("gmailbutton").addEventListener("click", greenbutton);
    document.getElementById("generatorbutton").addEventListener("click", greenbutton);

    // parse url 
    parseUrl()

    // create initial preview
    update();
}

/**
 * Reset the form by reloading the page with original query
 */
function resetForm() {
    window.location.replace(originalurl);
}

/**
 * parse the url and set the form accordingly
 */
function parseUrl() {
    // parse url 
    // https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
    var urlParams;
    (window.onpopstate = function () {
        var match,
            pl = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query = window.location.search.substring(1);

        urlParams = {};
        while (match = search.exec(query))
            urlParams[decode(match[1])] = decode(match[2]);
    })();

    //set form
    let basics = function (basic) {
        if (basic in urlParams) {
            form[basic].value = urlParams[basic];
        }
    }
    basics("fullname");
    basics("position");
    basics("entityname");
    basics("weblink");
    if ("contact" in urlParams) {
        let contactUrl = urlParams["contact"].replace(/%%%/, '').split("%%%");
        contactRowContainer.innerHTML = ""; //remove all existing rows
        for (let i = 0; i < contactUrl.length; i = i + 2) { //add rows
            addRow(contactUrl[i], contactUrl[i + 1]);
        }
    }
    let socialmedia = function (platform) {
        if (platform + "true" in urlParams) {
            form[platform + "-checkbox"].checked = true;
            form[platform].value = urlParams[platform + "true"];
        } else if (platform + "false" in urlParams) {
            form[platform + "-checkbox"].checked = false;
            checkboxChanged(form[platform + "-checkbox"]);
            form[platform].value = urlParams[platform + "false"];
        }
    }
    socialmedia("webtext");
    socialmedia("linkedin");
    socialmedia("facebook");
    socialmedia("instagram");
}


/**
 * making button green for 2sek
 */
function greenbutton() {
    let button = this;
    button.classList.add("buttonsuccess");
    let buttontext = button.innerHTML;
    button.innerHTML = "Done!"
    setTimeout(function () {
        button.classList.remove("buttonsuccess");
        button.innerHTML = buttontext;
    }, 2000)
}

/**
 * Toggle the style how the checkbox row is displayed
 */
function checkboxChanged(checkbox) {
    let label = checkbox.parentNode.nextElementSibling;
    let input = label.nextElementSibling.firstElementChild;
    if (checkbox.checked) {
        label.classList.remove("grey");
        input.classList.remove("grey");
        input.readOnly = false;
    }
    else {
        label.classList.add("grey");
        input.classList.add("grey");
        input.readOnly = true;
    }
}

/**
 * Add a contact information line above the "Add line" button
 */
function addRow(type = "empty", value = "") {
    //template
    let rowInnerHtml = '' +
        '    <div class="col col-label">' +
        '        <select style = "width: 100%;" >' +
        '            <option value="email">Email</option>' +
        '            <option value="phone">Phone</option>' +
        '            <option value="address">Address</option>' +
        '            <option value="skype">Skype</option>' +
        '            <option value="empty">(no icon)</option>' +
        '       </select >' +
        '    </div >' +
        '    <div class="col col-input-x">' +
        '        <input type="text" value="%%">' +
        '    </div>' +
        '    <div class="col col-x">' +
        '        <button type="button" class="xbutton" onclick="removeRow(this)">X</button>' +
        '    </div>';
    rowInnerHtml = rowInnerHtml.replace(type + '"', type + '" selected');
    rowInnerHtml = rowInnerHtml.replace(/%%/, value);

    // create and append new row
    let row = document.createElement("div");
    row.className = "row";
    row.innerHTML = rowInnerHtml;
    document.getElementById("contact-row-container").appendChild(row);
}

/**
 * Remove the contact information line corresponding to the button
 */
function removeRow(button) {
    // remove row
    let row = button.parentNode.parentNode;
    row.remove();
}

/**
 * Update the resulting signature in the iframe and the textfield with the values from the form.
 */
function update() {
    // Read basic values from form
    let fullname = form["fullname"].value;
    let position = form["position"].value;
    let entityname = form["entityname"].value; // entity name as in list
    let entitystring = entityname.replace(/ /g, '-').replace(/\./g, ''); // entity name with "-" instead of " " because Gmail doesn't support whitespaces. Remove dots.

    // Read contact information values from form and generate table rows
    let nRows = contactRowContainer.childElementCount;
    let contactRowTemplate = `
        <tr>
            <td style="height:24px; width:20px; padding-right: 10px;" width="30">
                %%img%%
            </td>
            <td style="height:24px;">
                <a %%href%%
                    style="color: #333333 !important; %%text-decoration%%">
                    <span
                        style="color: #333333 !important; %%text-decoration%%">
                        <font color="#333333">
                            %%value%%
                        </font>
                    </span>
                </a>
            </td>
        </tr>
        `;
    let imgTemplate = '<img alt="" style="width:20px; height:20px; border:0;" src="%%path%%%%type%%.png" width="20" height="20" border="0">';
    let contactRowsConcat = "";
    let contactUrl = "";
    for (let i = 0; i < nRows; i++) {
        let value = contactRowContainer.children[i].firstElementChild.nextElementSibling.firstElementChild.value;
        let type = contactRowContainer.children[i].firstElementChild.firstElementChild.value;
        let contactRow = contactRowTemplate;
        switch (type) {
            case "email":
                contactRow = contactRow.replace(/%%text-decoration%%/g, 'text-decoration: underline !important;');
                contactRow = contactRow.replace(/%%href%%/, 'href="mailto:%%value%%"');
                contactRow = contactRow.replace(/%%img%%/, imgTemplate);
                break;
            case "phone":
                contactRow = contactRow.replace(/%%text-decoration%%/g, 'text-decoration: underline !important;');
                contactRow = contactRow.replace(/%%href%%/, 'href="tel:%%value%%"');
                contactRow = contactRow.replace(/%%img%%/, imgTemplate);
                break;
            case "address":
                contactRow = contactRow.replace(/%%text-decoration%%/g, ''); //no underline
                contactRow = contactRow.replace(/%%href%%/, ''); //no href
                contactRow = contactRow.replace(/%%img%%/, imgTemplate);
                break;
            case "skype":
                contactRow = contactRow.replace(/%%text-decoration%%/g, 'text-decoration: underline !important;');
                contactRow = contactRow.replace(/%%href%%/, 'href="skype:%%value%%');
                contactRow = contactRow.replace(/%%img%%/, imgTemplate);
                break;
            case "empty":
                contactRow = contactRow.replace(/%%text-decoration%%/g, ''); //no underline
                contactRow = contactRow.replace(/%%href%%/, ''); //no href
                contactRow = contactRow.replace(/%%img%%/, ''); //no img
                break;
        }
        contactRow = contactRow.replace(/%%value%%/g, value);
        contactRow = contactRow.replace(/%%type%%/g, type);
        contactRowsConcat = contactRowsConcat + contactRow;

        //for encoded url
        contactUrl += '%%%' + type + '%%%' + value;
    }

    // Read social media values from form and generate icons
    let weblink = form["weblink"].value;
    let webtext = ""
    if (form["webtext-checkbox"].checked) {
        webtext = form["webtext"].value;
    }
    let socialTemplate = `<a href="%%link%%" target="_blank" rel="noopener"><img alt=""
        style="width:20px; height:20px; border:0; text-align: center;"
        src="%%path%%%%platform%%.png" width="20" height="20"
        border="0"></a>`;
    let spacer = '<span>&nbsp;</span>'
    let socialConcat = spacer;
    if (form["linkedin-checkbox"].checked) {
        let html = socialTemplate.replace(/%%link%%/, form["linkedin"].value);
        html = html.replace(/%%platform%%/, "linkedin");
        socialConcat = socialConcat + html + spacer;
    }
    if (form["facebook-checkbox"].checked) {
        let html = socialTemplate.replace(/%%link%%/, form["facebook"].value);
        html = html.replace(/%%platform%%/, "facebook");
        socialConcat = socialConcat + html + spacer;
    }
    if (form["instagram-checkbox"].checked) {
        let html = socialTemplate.replace(/%%link%%/, form["instagram"].value);
        html = html.replace(/%%platform%%/, "instagram");
        socialConcat = socialConcat + html + spacer;
    }

    // get current date
    let utc = new Date().toJSON().slice(0, 10).replace(/-/g, '/');

    // Replace placeholders in original html string with the actual values
    let newstring = original.value.replace(/%%fullname%%/g, fullname);
    newstring = newstring.replace(/%%position%%/, position);
    newstring = newstring.replace(/%%entityname%%/, entityname);
    newstring = newstring.replace(/%%logo%%/, entitystring);
    newstring = newstring.replace(/%%contactrows%%/, contactRowsConcat);
    newstring = newstring.replace(/%%socialmedia%%/, socialConcat);
    newstring = newstring.replace(/%%weblink%%/g, weblink);
    newstring = newstring.replace(/%%webtext%%/, webtext);
    newstring = newstring.replace(/%%timestamp%%/, utc);
    newstring = newstring.replace(/%%path%%/g, path);

    // Set the html sting as value of the result textbox and write it to the result iframe
    result.value = newstring;
    iframe.contentDocument.open();
    iframe.contentDocument.write(newstring);
    iframe.contentDocument.close();

    // Fix iframe height
    iframe.height = iframe.contentWindow.document.body.scrollHeight + 5;

    // create and add url querystring
    let urlstring = "?";
    urlstring += "fullname=" + encodeURIComponent(fullname) + "&" +
        "position=" + encodeURIComponent(position) + "&" +
        "entityname=" + encodeURIComponent(entityname) + "&" +
        "contact=" + encodeURIComponent(contactUrl) + "&" +
        "weblink=" + encodeURIComponent(weblink) + "&" +
        "webtext" + form["webtext-checkbox"].checked + "=" + encodeURIComponent(form["webtext"].value) + "&" +
        "linkedin" + form["linkedin-checkbox"].checked + "=" + encodeURIComponent(form["linkedin"].value) + "&" +
        "facebook" + form["facebook-checkbox"].checked + "=" + encodeURIComponent(form["facebook"].value) + "&" +
        "instagram" + form["instagram-checkbox"].checked + "=" + encodeURIComponent(form["instagram"].value);
    window.history.replaceState(null, null, urlstring); //not reloading page
}

/**
 * Copy the generator link (the current url) to the clipboard
 */
function copyGeneratorLink() {
    var dummy = document.createElement('input'),
        text = window.location.href;
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
}

/**
  * Copy the signature as HTML element (not as plain text) from the
  * result iframe, so that in can be pasted to the HTML editor textfield in Gmail.
  */
function copygmail() {

    // Select all and copy to clipboard. For this, the document needs to be in designMode.
    // Note: execCommand is obsolete, but this seemed to work best this way...
    iframe.contentDocument.designMode = "on";
    iframe.contentDocument.execCommand("selectAll", false, null);
    iframe.contentDocument.execCommand("copy", false, null);
    iframe.contentDocument.designMode = "off";

    // Clear the selection
    if (iframe.contentWindow.getSelection) {
        if (iframe.contentWindow.getSelection().empty) { // Chrome
            iframe.contentWindow.getSelection().empty();
        } else if (iframe.contentWindow.getSelection().removeAllRanges) { // Firefox
            iframe.contentWindow.getSelection().removeAllRanges();
        }
    } else if (iframe.contentDocument.selection) { // IE?
        iframe.contentDocument.selection.empty();
    }
}

/**
 * Copy the HTML code of the signature to the clipboard as plain text.
 */
function copyhtml() {

    // Select and copy (the textfield is already editable / in designMode)
    // Note: execCommand is obsolete
    result.select();
    document.execCommand("copy");

    // Clear selection
    result.setSelectionRange(0, 0)
}


/**
 * Scales down the iframe with the resulting signature so it fits small screens. Event listeners (onresize and onload) below.
 */
function setFrameZoom() {
    let iframewrapper = document.getElementById("iframeresultwrapper");
    let iframeresult = document.getElementById("iframeresult");
    let ww = parseInt(window.getComputedStyle(iframewrapper).width.replace("px", ""));
    if (ww < 485) {
        iframeresult.style.transformOrigin = "0 0";
        iframeresult.style.transform = "scale(" + ww / 495 + ")";
    }
}
window.addEventListener("resize", setFrameZoom);
window.addEventListener("load", setFrameZoom);
